<?php

namespace Banovic\OrderNote\Controller\Adminhtml\Note;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Banovic_OrderNote::ordernote';

    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Banovic_OrderNote::ordernote');
        $resultPage->getConfig()->getTitle()->prepend(__('Order Notes'));

        return $resultPage;
    }
}
