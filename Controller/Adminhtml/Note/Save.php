<?php

namespace Banovic\OrderNote\Controller\Adminhtml\Note;

use Magento\Backend\App\Action;
use Banovic\OrderNote\Model\Note;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Banovic_OrderNote::save';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Banovic\OrderNote\Model\NoteFactory
     */
    private $noteFactory;

    /**
     * @var \Banovic\OrderNote\Api\NoteRepositoryInterface
     */
    private $noteRepository;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Banovic\OrderNote\Model\NoteFactory $noteFactory
     * @param \Banovic\OrderNote\Api\NoteRepositoryInterface $noteRepository
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Banovic\OrderNote\Model\NoteFactory $noteFactory,
        \Banovic\OrderNote\Api\NoteRepositoryInterface $noteRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->noteFactory = $noteFactory;
        $this->noteRepository = $noteRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            if (empty($data['id'])) {
                $data['id'] = null;
            }

            /** @var \Banovic\OrderNote\Model\Note $model */
            $model = $this->noteFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->noteRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This order note no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->noteRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the order note.'));
                $this->dataPersistor->clear('order_note');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?:$e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the order note.'));
            }

            $this->dataPersistor->set('order_note', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
