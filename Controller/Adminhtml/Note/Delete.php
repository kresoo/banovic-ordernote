<?php

namespace Banovic\OrderNote\Controller\Adminhtml\Note;

use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Banovic_OrderNote::delete';

    protected $_noteFactory;

    public function __construct(
        Action\Context $context,
        \Banovic\OrderNote\Model\NoteFactory $noteFactory
    ) {
        $this->_noteFactory = $noteFactory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if ($id) {
            try {
                // init model and delete
                $model = $this->_noteFactory->create();
                $model->load($id);

                $model->delete();
                
                // display success message
                $this->messageManager->addSuccessMessage(__('The note has been deleted.'));
                
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find a note to delete.'));

        return $resultRedirect->setPath('*/*/');
    }
}
