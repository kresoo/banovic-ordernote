<?php

namespace Banovic\OrderNote\Controller\Index;

use Magento\Framework\App\RequestInterface;

class Save extends \Magento\Framework\App\Action\Action
{

    protected $_customerSession;

    protected $_customerNoteResource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Banovic\OrderNote\Model\ResourceModel\Customer\Note $customerNoteResource
    ){
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_customerNoteResource = $customerNoteResource;
    }

    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    public function execute()
    {
        try {
            $noteText = $this->getRequest()->getParam('note-text');
            $customerId = $this->_customerSession->getCustomerId();


            $this->_customerNoteResource->saveCustomerNote($customerId, $noteText);

            $this->messageManager->addSuccess(__('Your note has been saved'));

        } catch (\Exception $e) {
            $this->messageManager->addError($e);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
}
