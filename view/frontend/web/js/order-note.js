define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote'
], function ($, Component, ko, customer) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Banovic_OrderNote/order-note',
            noteSelectOptions: null,
            note: ko.observable(),
        },

        initialize: function () {
            this._super()
            this.initProperties();

            return this;
        },

        initProperties: function() {

            this.noteSelectOptions = window.checkoutConfig.orderNote.selectOptions;

            if(window.checkoutConfig.quoteData['order_note']) {
                this.note(window.checkoutConfig.quoteData['order_note']);
            } else if(customer.customerData['extension_attributes']){
                this.note(customer.customerData['extension_attributes']['default_order_note']);
            }
        },

        changedNoteSelection: function (obj, event) {
            var selectedOptionValue = event.target.value;
            var selectedOption = $('#order-note-select option[value="' + selectedOptionValue + '"]');
            var note = selectedOption && selectedOption.text() ? selectedOption.text() : '';
            this.note(note);
        }
    });
});
