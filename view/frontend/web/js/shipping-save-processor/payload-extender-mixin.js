define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
    'use strict';

    return function (payloadAction) {

        /** Override default payload action and add order_note to request */
        return wrapper.wrap(payloadAction, function (originalAction, payload) {
            originalAction(payload);

            if (payload.addressInformation['extension_attributes'] === undefined) {
                payload.addressInformation['extension_attributes'] = {};
            }

            var orderNoteInput = $('#order-note-text');
            if(orderNoteInput) {
                payload.addressInformation['extension_attributes']['order_note'] = orderNoteInput.val();
            }
        });
    };
});
