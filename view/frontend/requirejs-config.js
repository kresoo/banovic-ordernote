var config = {
    map: {
        '*': {
            'Magento_Checkout/js/view/shipping' : 'Banovic_OrderNote/js/shipping-override'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': {
                'Banovic_OrderNote/js/shipping-save-processor/payload-extender-mixin': true
            }
        }
    }
};