<?php
namespace Banovic\OrderNote\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveNoteToOrderObserver implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $note = $observer->getEvent()->getQuote()->getOrderNote();

        $observer->getEvent()->getOrder()->setOrderNote($note);

        return $this;
    }

}