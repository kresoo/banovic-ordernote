<?php

namespace Banovic\OrderNote\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class CreateShipmentsCommand extends Command
{

    protected $_orderFactory;

    protected $_state;

    protected $_shipmentLoader;

    protected $_objectManager;

    protected $_helper;

    public function __construct(
        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
        \Banovic\OrderNote\Helper\Data $helper,
        $name = null
    ){
        parent::__construct($name);
        $this->_orderFactory = $orderFactory;
        $this->_state = $state;
        $this->_shipmentLoader = $shipmentLoader;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_helper = $helper;
    }

    protected function configure()
    {
        $this->setName('banovic_ordernote:createShipments')->setDescription('Create shipments for orders in processing state');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!$this->_helper->getLock(\Banovic\OrderNote\Helper\Data::CREATE_LOCK_NAME)) {
            return;
        }

        $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

        $collection = $this->_orderFactory->create()->getCollection();
        $collection->addFieldToFilter('state', \Magento\Sales\Model\Order::STATE_PROCESSING);
        $collection->setPageSize(20);

        $currentPage = 1;
        $collection->setCurPage($currentPage);
        $pageCount = $collection->getLastPageNumber();

        // process in batches
        while($currentPage <= $pageCount) {
            $currentPage++;

            foreach ($collection->getItems() as $order){
                try {
                    $this->_shipmentLoader->setOrderId($order->getEntityId());
                    $shipment = $this->_shipmentLoader->load();

                    if(!$shipment) {
                        continue;
                    }

                    $shipment->register();

                    $shipment->getOrder()->setIsInProcess(true);

                    $transaction = $this->_objectManager->create(\Magento\Framework\DB\Transaction::class);
                    $transaction->addObject(
                        $shipment
                    )->addObject(
                        $shipment->getOrder()
                    )->save();

                } catch (\Exception $e) {
                    continue;
                }
            }

            $collection->clear();
            $collection->setCurPage($currentPage);
        }

        $this->_helper->releaseLock(\Banovic\OrderNote\Helper\Data::CREATE_LOCK_NAME);
    }
}