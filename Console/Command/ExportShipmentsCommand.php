<?php

namespace Banovic\OrderNote\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class ExportShipmentsCommand extends Command
{
    protected $_state;

    protected $_shipment;

    protected $_dirList;

    protected $_ioFile;

    protected $_csv;

    protected $_localeDate;

    protected $_resource;

    protected $_helper;

    CONST SHIPMENT_IS_EXPORTED = 1;
    CONST SHIPMENT_IS_NOT_EXPORTED = 0;

    public function __construct(
        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\Order\Shipment $shipment,
        \Magento\Framework\Filesystem\DirectoryList $dirList,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\App\ResourceConnection $resource,
        \Banovic\OrderNote\Helper\Data $helper,
        $name = null
    ){
        parent::__construct($name);
        $this->_state = $state;
        $this->_shipment = $shipment;
        $this->_dirList = $dirList;
        $this->_ioFile = $ioFile;
        $this->_csv = $csv;
        $this->_localeDate = $localeDate;
        $this->_resource = $resource;
        $this->_helper = $helper;
    }

    protected function configure()
    {
        $this->setName('banovic_ordernote:exportShipments')->setDescription('Export shipments that have not yet been exported');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        if(!$this->_helper->getLock(\Banovic\OrderNote\Helper\Data::EXPORT_LOCK_NAME)) {
            return;
        }

        $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

        $collection = $this->_shipment->getCollection();
        $collection->getSelect()->joinLeft(
            ['so' => 'sales_order'],
            'main_table.order_id = so.entity_id',
            ['order_increment_id' => 'so.increment_id', 'order_note']
        );
        $collection->addFieldToFilter('is_exported', self::SHIPMENT_IS_NOT_EXPORTED);
        $collection->setPageSize(20);

        $exportedShipments = [];
        $data = [];
        foreach ($collection->getItems() as $shipment) {

            $data[] = [$shipment->getIncrementId(), $shipment->getOrderIncrementId(), $shipment->getOrderNote()];

            $exportedShipments[] = $shipment->getEntityId();
        }

        if($data) {
            try {
                $this->_exportShipments($data);

                if($exportedShipments) {
                    $this->_updateShipmentsExportStatus($exportedShipments);
                }
            } catch (\Exception $e) {

            }
        }

        $this->_helper->releaseLock(\Banovic\OrderNote\Helper\Data::EXPORT_LOCK_NAME);

        return $this;
    }

    protected function _exportShipments($data)
    {
        $headers = ['Shipment Increment ID', 'Order Increment ID', 'Order Note'];
        array_unshift($data, $headers);

        $exportDir = $this->_dirList->getPath('var') . '/export/order_note/';
        if(!is_dir($exportDir)) {
            $this->_ioFile->mkdir($exportDir);
        }

        $file = $exportDir . $this->_localeDate->date()->format('dmY\THi') . '.csv';

        $this->_csv->saveData($file, $data);
    }

    protected function _updateShipmentsExportStatus($exportedShipments)
    {
        $connection = $this->_resource->getConnection();
        $connection->update(
            $this->_resource->getTableName('sales_shipment'),
            ['is_exported' => self::SHIPMENT_IS_EXPORTED],
            ['entity_id IN (?)' => $exportedShipments]
        );

    }
}