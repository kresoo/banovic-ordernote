<?php

namespace Banovic\OrderNote\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Order note CRUD interface.
 * @api
 * @since 100.0.2
 */
interface NoteRepositoryInterface
{
    /**
     * Save note.
     *
     * @param \Banovic\OrderNote\Api\Data\NoteInterface $note
     * @return \Banovic\OrderNote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Banovic\OrderNote\Api\Data\NoteInterface $note);

    /**
     * Retrieve note.
     *
     * @param int $id
     * @return \Banovic\OrderNote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve order notes matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Banovic\OrderNote\Api\Data\NoteSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete order note.
     *
     * @param \Banovic\OrderNote\Api\Data\NoteInterface $note
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Banovic\OrderNote\Api\Data\NoteInterface $note);

    /**
     * Delete note by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
