<?php

namespace Banovic\OrderNote\Api\Data;

/**
 * Order note interface.
 * @api
 * @since 100.0.2
 */
interface NoteInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                  = 'id';
    const TEXT               = 'text';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get text
     *
     * @return string
     */
    public function getText();
}
