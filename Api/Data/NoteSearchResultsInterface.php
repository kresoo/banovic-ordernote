<?php

namespace Banovic\OrderNote\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for order note search results.
 * @api
 * @since 100.0.2
 */
interface NoteSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get order note list.
     *
     * @return \Banovic\OrderNote\Api\Data\NoteInterface[]
     */
    public function getItems();

    /**
     * Set order note list.
     *
     * @param \Banovic\OrderNote\Api\Data\NoteInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
