<?php

namespace Banovic\OrderNote\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_resource;

    CONST EXPORT_LOCK_NAME = 'export_shipments';
    CONST CREATE_LOCK_NAME = 'create_shipments';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        parent::__construct($context);
        $this->_resource = $resource;
    }

    public function getLock($name)
    {
        return (bool)$this->_resource->getConnection()->query("SELECT GET_LOCK(?, ?);", array($name, 1))->fetchColumn();;
    }

    public function releaseLock($name)
    {
        return (bool)$this->_resource->getConnection()->query("SELECT RELEASE_LOCK(?);", array($name))->fetchColumn();
    }
}