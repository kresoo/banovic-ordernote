<?php

namespace Banovic\OrderNote\Block\Adminhtml\Order\View;

class Note extends \Magento\Framework\View\Element\Template
{

    protected $_registry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_registry = $registry;
    }

    public function getNote()
    {
        $order = $this->_registry->registry('current_order');

        if(!$order) {
            return false;
        }

        return $order->getOrderNote() ? $order->getOrderNote() : false;
    }
}