<?php

namespace Banovic\OrderNote\Block\Adminhtml\Note\Edit;

use Magento\Backend\Block\Widget\Context;
use Banovic\OrderNote\Api\NoteRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var NoteRepositoryInterface
     */
    protected $noteRepository;

    /**
     * @param Context $context
     * @param NoteRepositoryInterface $noteRepository
     */
    public function __construct(
        Context $context,
        NoteRepositoryInterface $noteRepository
    ) {
        $this->context = $context;
        $this->noteRepository = $noteRepository;
    }

    /**
     * Return order note ID
     *
     * @return int|null
     */
    public function getNoteId()
    {
        try {
            return $this->noteRepository->getById($this->context->getRequest()->getParam('id'))->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
