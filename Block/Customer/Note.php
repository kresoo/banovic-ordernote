<?php

namespace Banovic\OrderNote\Block\Customer;

class Note extends \Magento\Framework\View\Element\Template
{

    protected $_customerNoteResource;

    protected $_customerSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Banovic\OrderNote\Model\ResourceModel\Customer\Note $customerNoteResource,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_customerNoteResource = $customerNoteResource;
        $this->_customerSession = $customerSession;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('My Order Note'));
    }

    public function getCustomerNote()
    {
        try {
            $customerId = $this->_customerSession->getCustomerId();

            return $this->_customerNoteResource->getCustomerNoteById($customerId);
        } catch (\Exception $e) {
            return false;
        }
    }
}