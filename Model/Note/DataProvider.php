<?php

namespace Banovic\OrderNote\Model\Note;

use Banovic\OrderNote\Model\ResourceModel\Note\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Banovic\OrderNote\Model\ResourceModel\Note\Collection;
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $noteCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $noteCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $noteCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var $page \Banovic\OrderNote\Model\Note */
        foreach ($items as $note) {
            $this->loadedData[$note->getId()] = $note->getData();
        }

        $data = $this->dataPersistor->get('order_note');
        if (!empty($data)) {
            $note = $this->collection->getNewEmptyItem();
            $note->setData($data);
            $this->loadedData[$note->getId()] = $note->getData();
            $this->dataPersistor->clear('order_note');
        }

        return $this->loadedData;
    }
}
