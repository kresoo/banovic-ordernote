<?php

namespace Banovic\OrderNote\Model\ResourceModel\Note;

/**
 * Order note collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Banovic\OrderNote\Model\Note::class, \Banovic\OrderNote\Model\ResourceModel\Note::class);
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
    }
}
