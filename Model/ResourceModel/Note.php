<?php

namespace Banovic\OrderNote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * order note mysql resource
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Note extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('banovic_order_note', 'id');
    }
}
