<?php

namespace Banovic\OrderNote\Model\ResourceModel\Customer;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * order note mysql resource
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Note extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('banovic_customer_order_note', 'id');
    }

    public function saveCustomerNote($customerId, $noteText)
    {
        $this->getConnection()->insertOnDuplicate($this->getMainTable(), ['customer_id' => $customerId, 'text' => $noteText], ['text']);
    }

    public function getCustomerNoteById($customerId)
    {
        $connection = $this->getConnection();

        $select = $connection
            ->select()
            ->from($this->getMainTable(), 'text')
            ->where('customer_id = ?', $customerId);

        $result = $connection->fetchOne($select);

        return $result ? $result : false;
    }
}
