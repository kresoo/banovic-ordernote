<?php

namespace Banovic\OrderNote\Model;

use Magento\Framework\Model\AbstractModel;

use Banovic\OrderNote\Api\Data\NoteInterface;

class Note extends AbstractModel implements NoteInterface
{
    protected function _construct()
    {
        $this->_init(\Banovic\OrderNote\Model\ResourceModel\Note::class);
    }

    public function getId()
    {
        return $this->getData(self::ID);
    }

    public function getText()
    {
        return $this->getData(self::TEXT);
    }
}