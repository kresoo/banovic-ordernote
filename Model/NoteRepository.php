<?php

namespace Banovic\OrderNote\Model;

use Banovic\OrderNote\Api\Data;
use Banovic\OrderNote\Api\NoteRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Banovic\OrderNote\Model\ResourceModel\Note as ResourceNote;
use Banovic\OrderNote\Model\ResourceModel\Note\CollectionFactory as NoteCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PageRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NoteRepository implements NoteRepositoryInterface
{
    /**
     * @var ResourceNote
     */
    protected $resource;

    /**
     * @var NoteFactory
     */
    protected $noteFactory;

    /**
     * @var NoteCollectionFactory
     */
    protected $noteCollectionFactory;

    /**
     * @var Data\NoteSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Banovic\OrderNote\Api\Data\NoteInterfaceFactory
     */
    protected $dataNoteFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param ResourceNote $resource
     * @param NoteFactory $pageFactory
     * @param Data\NoteInterfaceFactory $dataNoteFactory
     * @param NoteCollectionFactory $noteCollectionFactory
     * @param Data\NoteSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceNote $resource,
        NoteFactory $noteFactory,
        Data\NoteInterfaceFactory $dataNoteFactory,
        NoteCollectionFactory $noteCollectionFactory,
        Data\NoteSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->noteFactory = $noteFactory;
        $this->noteCollectionFactory = $noteCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataNoteFactory = $dataNoteFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
    }

    /**
     * Save Order NOte data
     *
     * @param \Banovic\OrderNote\Api\Data\NoteInterface $note
     * @return Note
     * @throws CouldNotSaveException
     */
    public function save(\Banovic\OrderNote\Api\Data\NoteInterface $note)
    {
        try {
            $this->resource->save($note);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the order note: %1', $exception->getMessage()),
                $exception
            );
        }
        return $note;
    }

    /**
     * Load Order Note data by given note id
     *
     * @param string $id
     * @return Note
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $note = $this->noteFactory->create();
        $note->load($id);
        if (!$note->getId()) {
            throw new NoSuchEntityException(__('Order note with id "%1" does not exist.', $id));
        }
        return $note;
    }

    /**
     * Load Note data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magento\Cms\Api\Data\PageSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Banovic\OrderNote\Model\ResourceModel\Note\Collection $collection */
        $collection = $this->noteCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\PageSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete Note
     *
     * @param \Banovic\OrderNote\Api\Data\NoteInterface $note
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Banovic\OrderNote\Api\Data\NoteInterface $note)
    {
        try {
            $this->resource->delete($note);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the order note: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete Note by given note id
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * Retrieve collection processor
     *
     * @deprecated 101.1.0
     * @return CollectionProcessorInterface
     */
    private function getCollectionProcessor()
    {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                'Magento\Cms\Model\Api\SearchCriteria\PageCollectionProcessor'
            );
        }
        return $this->collectionProcessor;
    }
}
