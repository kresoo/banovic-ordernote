<?php

namespace Banovic\OrderNote\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class OrderNoteSelectOptionsProvider implements ConfigProviderInterface
{

    protected $_noteRepository;

    protected $_searchCriteriaBuilder;

    public function __construct(
        \Banovic\OrderNote\Model\NoteRepository $noteRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->_noteRepository = $noteRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getConfig()
    {
        return [
            'orderNote' => [
                'selectOptions' => $this->_formatSelectOptions()
            ]
        ];
    }

    protected function _formatSelectOptions()
    {
        $notes = $this->_noteRepository->getList($this->_searchCriteriaBuilder->create());

        $options = [];
        $options[] = ['id' => null, 'text' => ''];
        foreach ($notes->getItems() as $note) {
            $options[] = ['id' => $note->getId(), 'text' => $note->getText()];
        }

        return $options;
    }
}