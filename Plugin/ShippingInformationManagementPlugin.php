<?php

namespace Banovic\OrderNote\Plugin;

class ShippingInformationManagementPlugin
{
    protected $_quoteRepository;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->_quoteRepository = $quoteRepository;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $orderNote = $extAttributes->getOrderNote();
        $quote = $this->_quoteRepository->getActive($cartId);
        $quote->setOrderNote($orderNote);
    }
}
