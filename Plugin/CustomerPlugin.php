<?php

namespace Banovic\OrderNote\Plugin;

use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Customer\Api\Data\CustomerExtensionInterface;
use Banovic\OrderNote\Model\ResourceModel\Customer\Note;

class CustomerPlugin
{

    protected $_customerNoteResource;

    public function __construct(
        ExtensionAttributesFactory $extensionFactory,
        Note $customerNoteResource
    ) {
        $this->extensionFactory = $extensionFactory;
        $this->_customerNoteResource = $customerNoteResource;
    }

    public function afterGetById(CustomerRepository $subject, CustomerInterface $customer)
    {
        $extensionAttributes = $customer->getExtensionAttributes();
        if ($extensionAttributes === null) {
            /** @var CustomerExtensionInterface $extensionAttributes */
            $extensionAttributes = $this->extensionFactory->create(CustomerInterface::class);
            $customer->setExtensionAttributes($extensionAttributes);
        }
        if ($extensionAttributes->getDefaultOrderNote() === null) {
            $defaultOrderNote = $this->_customerNoteResource->getCustomerNoteById($customer->getId());
            $extensionAttributes->setDefaultOrderNote($defaultOrderNote);
        }
        return $customer;
    }
}
