<?php

namespace Banovic\OrderNote\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->_createCustomerNoteTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->_addOrderNoteColumnToQuoteAndOrder($setup);
        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->_addIsExportedColumnToShipment($setup);
        }

        $setup->endSetup();
    }

    protected function _createCustomerNoteTable(SchemaSetupInterface $installer)
    {
        /**
         * Create table 'banovic_customer_order_note'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('banovic_customer_order_note')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Customer ID'
        )->addColumn(
            'text',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'Customer Note Text'
        )->addForeignKey(
            $installer->getFkName('banovic_customer_order_note', 'customer_id', 'customer_entity', 'entity_id'),
            'customer_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName(
                'banovic_customer_order_note',
                ['customer_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['customer_id'],
            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'Order Note Table'
        );

        $installer->getConnection()->createTable($table);
    }

    protected function _addOrderNoteColumnToQuoteAndOrder(SchemaSetupInterface $installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'order_note',
            [
                'nullable' => true,
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'   => 255,
                'comment'  => 'Order note'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'order_note',
            [
                'nullable' => true,
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'   => 255,
                'comment'  => 'Order note'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'order_note',
            [
                'nullable' => true,
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'   => 255,
                'comment'  => 'Order note'
            ]
        );

    }

    public function _addIsExportedColumnToShipment(SchemaSetupInterface $installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_shipment'),
            'is_exported',
            [
                'nullable' => false,
                'default' => 0,
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment'  => 'Is Exported'
            ]
        );
    }
}
