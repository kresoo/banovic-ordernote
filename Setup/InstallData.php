<?php

namespace Banovic\OrderNote\Setup;

use Banovic\OrderNote\Model\NoteFactory;
use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Note factory
     *
     * @var NoteFactory
     */
    private $noteFactory;

    /**
     * Init
     *
     * @param NoteFactory\ $noteFactory
     */
    public function __construct(NoteFactory $noteFactory)
    {
        $this->noteFactory = $noteFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $orderNotes = [
            [
                'text' => 'Leave at the doorstep'
            ],
            [
                'text' => 'Leave in the backyard'
            ],
            [
                'text' => 'Ring longer'
            ]
        ];

        foreach ($orderNotes as $data) {
            $this->createNote()->setData($data)->save();
        }
    }

    /**
     * Create note
     *
     * @return Note
     */
    public function createNote()
    {
        return $this->noteFactory->create();
    }
}
