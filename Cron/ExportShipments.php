<?php

namespace Banovic\OrderNote\Cron;

class ExportShipments
{
    protected $_shell;

    protected $_phpExecutableFinder;

    public function __construct(
        \Magento\Framework\Process\PhpExecutableFinderFactory $phpExecutableFinderFactory,
        \Magento\Framework\App\Shell $shell
    ){
        $this->_phpExecutableFinder = $phpExecutableFinderFactory->create();
        $this->_shell = $shell;
    }

    public function execute()
    {
        $phpPath = $this->_phpExecutableFinder->find() ? : 'php';

        $command = $phpPath . ' ' . BP . '/bin/magento ' . ' banovic_ordernote:exportShipments';
        $this->_shell->execute($command);
    }
}
